import request from '@/utils/request'

// 查询备件关联文档中间列表
export function listDoc(query) {
  return request({
    url: '/system/doc/list',
    method: 'get',
    params: query
  })
}

// 查询备件关联文档中间详细
export function getDoc(partDocId) {
  return request({
    url: '/system/doc/' + partDocId,
    method: 'get'
  })
}

// 新增备件关联文档中间
export function addDoc(data) {
  return request({
    url: '/system/doc',
    method: 'post',
    data: data
  })
}

// 修改备件关联文档中间
export function updateDoc(data) {
  return request({
    url: '/system/doc',
    method: 'put',
    data: data
  })
}

// 删除备件关联文档中间
export function delDoc(partDocId) {
  return request({
    url: '/system/doc/' + partDocId,
    method: 'delete'
  })
}

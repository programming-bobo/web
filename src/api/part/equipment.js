import request from '@/utils/request'

// 查询备件-装备中间列表
export function listEquipment(query) {
  return request({
    url: '/system/equipment/list',
    method: 'get',
    params: query
  })
}

// 查询备件-装备中间详细
export function getEquipment(partEquId) {
  return request({
    url: '/system/equipment/' + partEquId,
    method: 'get'
  })
}

// 新增备件-装备中间
export function addEquipment(data) {
  return request({
    url: '/system/equipment',
    method: 'post',
    data: data
  })
}

// 修改备件-装备中间
export function updateEquipment(data) {
  return request({
    url: '/system/equipment',
    method: 'put',
    data: data
  })
}

// 删除备件-装备中间
export function delEquipment(partEquId) {
  return request({
    url: '/system/equipment/' + partEquId,
    method: 'delete'
  })
}

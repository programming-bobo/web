import request from '@/utils/request'

// 查询备件图片中间列表
export function listImg(query) {
  return request({
    url: '/system/img/list',
    method: 'get',
    params: query
  })
}

// 查询备件图片中间详细
export function getImg(partImgId) {
  return request({
    url: '/system/img/' + partImgId,
    method: 'get'
  })
}

// 新增备件图片中间
export function addImg(data) {
  return request({
    url: '/system/img',
    method: 'post',
    data: data
  })
}

// 修改备件图片中间
export function updateImg(data) {
  return request({
    url: '/system/img',
    method: 'put',
    data: data
  })
}

// 删除备件图片中间
export function delImg(partImgId) {
  return request({
    url: '/system/img/' + partImgId,
    method: 'delete'
  })
}

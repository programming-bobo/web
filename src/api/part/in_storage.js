import request from '@/utils/request'

// 查询备件入库列表
export function listStorage(query) {
  return request({
    url: '/part/in/storage/list',
    method: 'get',
    params: query
  })
}

// 查询备件入库详细
export function getStorage(inStorageId) {
  return request({
    url: '/part/in//storage/' + inStorageId,
    method: 'get'
  })
}

// 新增备件入库
export function addStorage(data) {
  return request({
    url: '/part/in//storage',
    method: 'post',
    data: data
  })
}

// 修改备件入库
export function updateStorage(data) {
  return request({
    url: '/part/in/storage',
    method: 'put',
    data: data
  })
}

// 删除备件入库
export function delStorage(inStorageId) {
  return request({
    url: '/part/in/storage/' + inStorageId,
    method: 'delete'
  })
}


// 通过申请
export function auditInStorage(inStorageId) {
  return request({
    url: '/part/in/storage/audit/' + inStorageId,
    method: 'get'
  })
}

// 驳回申请
export function rejectInStorage(inStorageId) {
  return request({
    url: '/part/in/storage/reject/' + inStorageId,
    method: 'get'
  })
}

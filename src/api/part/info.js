import request from '@/utils/request'

// 查询备件台账列表
export function listInfo(query) {
  return request({
    url: '/part/info/list',
    method: 'get',
    params: query
  })
}

// 查询备件台账详细
export function getInfo(partId) {
  return request({
    url: '/part/info/' + partId,
    method: 'get'
  })
}

// 新增备件台账
export function addInfo(data) {
  return request({
    url: '/part/info',
    method: 'post',
    data: data
  })
}

// 修改备件台账
export function updateInfo(data) {
  return request({
    url: '/part/info',
    method: 'put',
    data: data
  })
}

// 删除备件台账
export function delInfo(partId) {
  return request({
    url: '/part/info/' + partId,
    method: 'delete'
  })
}

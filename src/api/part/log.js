import request from '@/utils/request'

// 查询备件日志列表
export function listLog(query) {
  return request({
    url: '/system/log/list',
    method: 'get',
    params: query
  })
}

// 查询备件日志详细
export function getLog(partLogId) {
  return request({
    url: '/system/log/' + partLogId,
    method: 'get'
  })
}

// 新增备件日志
export function addLog(data) {
  return request({
    url: '/system/log',
    method: 'post',
    data: data
  })
}

// 修改备件日志
export function updateLog(data) {
  return request({
    url: '/system/log',
    method: 'put',
    data: data
  })
}

// 删除备件日志
export function delLog(partLogId) {
  return request({
    url: '/system/log/' + partLogId,
    method: 'delete'
  })
}

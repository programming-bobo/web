import request from '@/utils/request'

// 查询备件入库列表
export function listStorage(query) {
  return request({
    url: '/part/out/storage/list',
    method: 'get',
    params: query
  })
}

// 查询备件入库详细
export function getStorage(inStorageId) {
  return request({
    url: '/part/out/storage/' + inStorageId,
    method: 'get'
  })
}

// 新增备件入库
export function addStorage(data) {
  return request({
    url: '/part/out/storage',
    method: 'post',
    data: data
  })
}

// 修改备件入库
export function updateStorage(data) {
  return request({
    url: '/part/out/storage',
    method: 'put',
    data: data
  })
}

// 删除备件入库
export function delStorage(outStorageId) {
  return request({
    url: '/part/out/storage/' + outStorageId,
    method: 'delete'
  })
}
// 通过申请
export function auditStorage(outStorageId) {
  return request({
    url: '/part/out/storage/audit/' + outStorageId,
    method: 'get'
  })
}

// 驳回申请
export function rejectStorage(outStorageId) {
  return request({
    url: '/part/out/storage/reject/' + outStorageId,
    method: 'get'
  })
}

import request from '@/utils/request'

// 查询入库明细列表
export function listPart(query) {
  return request({
    url: '/system/part/list',
    method: 'get',
    params: query
  })
}

// 查询入库明细详细
export function getPart(inStorageDetailId) {
  return request({
    url: '/system/part/' + inStorageDetailId,
    method: 'get'
  })
}

// 新增入库明细
export function addPart(data) {
  return request({
    url: '/system/part',
    method: 'post',
    data: data
  })
}

// 修改入库明细
export function updatePart(data) {
  return request({
    url: '/system/part',
    method: 'put',
    data: data
  })
}

// 删除入库明细
export function delPart(inStorageDetailId) {
  return request({
    url: '/system/part/' + inStorageDetailId,
    method: 'delete'
  })
}

import request from '@/utils/request'

export function TypeList(data, method) {
  return request({
    url: '/part/type/list',
    method: method,
    data: data
  })
}

/**
 * 新增备件类型
 * @param data:新增的备件类型表单
 * @returns {*}
 */
export function addType(data) {
  return request({
    url: '/part/type',
    method: 'post',
    data: data
  })
}

/**
 * 修改备件类型
 * @param data:修改的备件类型表单
 * @returns {*}
 */
export function updateType(data) {
  return request({
    url: '/part/type',
    method: 'put',
    data: data
  })
}

/**
 * 批量删除备件类型
 * @param partTypeId:批量删除的备件类型的partTypeID数组
 * @returns {*}
 */
export function delType(partTypeId) {
  return request({
    url: '/part/type/' + partTypeId,
    method: 'delete'
  })
}
/**
 * 批量启用备件类型
 * @param partTypeId:批量启用的备件类型的partTypeID数组
 * @returns {*}
 */
export function enable(partTypeId) {
  return request({
    url: '/part/type/enable/' + partTypeId,
    method: 'get'
  })
}
/**
 * 批量禁用备件类型
 * @param partTypeId:批量禁用的备件类型的partTypeID数组
 * @returns {*}
 */
export function disable(partTypeId) {
  return request({
    url: '/part/type/disable/' + partTypeId,
    method: 'get'
  })
}

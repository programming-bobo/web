import request from '../../utils/request'

// 查询rep列表
export function listInfo(query) {
  return request({
    url: '/rep/info/list',
    method: 'post',
    params: query
  })
}

// 查询rep详细
export function getInfo(data) {
  return request({
    url: '/rep/info/edit',
    method: 'post',
    data: data
  })
}

// 新增rep
export function addInfo(data) {
  return request({
    url: '/rep/info/add',
    method: 'post',
    data: data
  })
}

// 修改rep
export function updateInfo(data) {
  return request({
    url: '/rep/info/edit',
    method: 'post',
    data: data
  })
}

// 删除rep
export function delInfo(repId) {
  return request({
    url: '/rep/info/remove/' + repId,
    method: 'post'
  })
}

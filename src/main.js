// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from "axios"
import store from "./store";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import * as echarts from 'echarts'
import 'lib-flexible/flexible.js'
import Print from 'vue-print-nb'
import jquery from 'jquery'
import BaiduMap from "vue-baidu-map";

Vue.use(BaiduMap, {
  ak: '5tSEZAEWFePXMqOoi0mOdr7ozcAG0e6G'
})

Vue.use(Print)
import fitPlugin from 'v-fit-columns';

Vue.prototype.$ = jquery

Vue.config.productionTip = false
//设置axios为Vue的属性$axios
Vue.prototype.$axios = axios
//定义echarts
Vue.prototype.$echarts = echarts
Vue.use(ElementUI);
Vue.use(fitPlugin);
//设置node跨域代理，打包时注释掉，生成的dist目录文件放入SpringBoot的static文件夹
axios.defaults.baseURL = 'api/'
// 添加请求拦截器，在请求头中加token
axios.interceptors.request.use(
  config => {
    config.headers.Authorization = store.state.token;
    return config
  },
  error => {
    return Promise.reject(error)
  })

// 添加响应拦截器,拦截判断是否登录
axios.interceptors.response.use(
  resp => {
    if (resp.data.code == 401) {
      store.state.token = '';
      router.push('Login').then();
    }
    return resp;
  },
  error => {
    return Promise.reject(error)
  })

// 全局前置守卫，初始化时执行，每次路由切换前执行
router.beforeEach((to,from,next)=>{
  next()
   // if(to.name == 'Login' || to.name == 'Home' || to.name == 'NoAuth' || to.name == 'SysUserProfile'){
   //   next();
   // }else if(store.state.authUrlsList.indexOf(to.name) > -1){
   //   next();
   // }else{
   //   router.push('NoAuth').then()
   // }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})

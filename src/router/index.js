import Vue from 'vue'
import Router from 'vue-router'
import Panel from "../components/Panel";
import Login from  '../components/Login'
import NoAuth from '../components/NoAuth'
import Home from  '../components/Home'
import Personal from '../components/personal/Personal'
import SysUserProfile from '../components/sys/SysUserProfile'
import SysUser from '../components/sys/SysUser'
import SysRole from '../components/sys/SysRole'
import SysAuth from '../components/sys/SysAuth'
import SysGroup from '../components/sys/SysGroup'
import CheckingPlan from "../components/checking/CheckingPlan";
import CheckingTask from "../components/checking/CheckingTask";
import ExecutionRule from "../components/checking/ExecutionRule";
import CheckingItem from "../components/checking/CheckingItem";
import RepList from "../components/rep/RepList";
import RepAdd from "../components/rep/RepAdd";
import MaintainPlan from "../components/maintain/MaintainPlan";
import maintainPlanItem from "../components/maintain/MaintainPlanItem";
import MaintainPlanAdd from "../components/maintain/MaintainPlanAdd";
import MaintainPlanDetail from "../components/maintain/MaintainPlanDetail";
import maintainPlanPart from "../components/maintain/MaintainPlanPart";
import maintainPlanDetail from "../components/maintain/MaintainPlanDetail";
import maintainPlanInfo from "../components/maintain/MaintainPlanInfo";
import maintainPlanEquipment from "../components/maintain/MaintainPlanEquipment";
import maintainPlanFile from "../components/maintain/MaintainPlanFile";
import maintainPlanLog from "../components/maintain/MaintainPlanLog";
import PartType from "../components/part/PartType";
import ExperienceItem from "../components/experience/experienceItem";
import RepDetail from "../components/rep/RepDetail";
import RepPlan from "../components/rep/RepPlan";
import RepCom from "../components/rep/RepCom";
import RepFile from "../components/rep/RepFile";
import EquType from "../components/equipment/EquType";
import EquipmentList from "../components/equipment/EquipmentList";
import PartInfo from "../components/part/PartInfo";
import PartInStorage from "../components/part/PartInStorage";
import PartOutStorage from "../components/part/PartOutStorage";
import ExperienceAdd from "../components/experience/ExperienceAdd";
import PartAdd from "../components/part/partInfo/PartAdd";
import PartInfoDetail from "../components/part/partInfo/PartInfoDetail";
import PartInfoCard from "../components/part/partInfo/PartInfoCard";
import PartInventory from "../components/part/partInfo/PartInventory";
import PartInStorageDetail from "../components/part/partInfo/PartInStorageDetail";
import PartOutStorageDetail from "../components/part/partInfo/PartOutStorageDetail";
import PartAssociatedEqu from "../components/part/partInfo/PartAssociatedEqu";
import PartImg from "../components/part/partInfo/PartImg";
import PartAssociatedDoc from "../components/part/partInfo/PartAssociatedDoc";
import PartOperateRecord from "../components/part/partInfo/PartOperateRecord";
import RepPart from "../components/rep/RepPart";
import RepAddP1 from "../components/rep/RepAddP1";
import Editor from "../components/experience/Editor";
import RepToDo from "../components/rep/RepToDo";
import RepDone from "../components/rep/RepDone";
import EquipmentInfo from "../components/equipment/EquipmentInfo";
import maintainTask from "../components/maintain/MaintainTask";
import OutStorageAdd from "../components/part/outStorage/OutStorageAdd";
import OutStorageDetail from "../components/part/outStorage/OutStorageDetail";
import OutStorageInfo from "../components/part/outStorage/OutStorageInfo";
import OutStoragePartInfo from "../components/part/outStorage/OutStoragePartInfo";
import OutStorageLog from "../components/part/outStorage/OutStorageLog";
import InStorageAdd from "../components/part/inStorage/InStorageAdd";
import InStorageDetail from "../components/part/inStorage/InStorageDetail";
import InStorageInfo from "../components/part/inStorage/InStorageInfo";
import InStoragePartInfo from "../components/part/inStorage/InStoragePartInfo";
import InStorageLog from "../components/part/inStorage/InStorageLog";
import EquipmentAdd from "../components/equipment/EquipmentAdd";
import MaintainItem from "../components/maintain/MaintainItem";
import MaintainLevel from "../components/maintain/MaintainLevel";
import MaintainTaskDetail from "../components/maintain/MaintainTaskDetail";
import MaintainTaskInfo from "../components/maintain/MaintainTaskInfo";
import RepLog from "../components/rep/RepLog";
import MaintainTaskItem from "../components/maintain/MaintainTaskItem";
import MaintainTaskPart from "../components/maintain/MaintainTaskPart";
import MaintainTaskFile from "../components/maintain/MaintainTaskFile";
import MaintainTaskLog from "../components/maintain/MaintainTaskLog";
import repUpload from "../components/rep/repUpload";
import MaintainTaskExecution from "../components/maintain/MaintainTaskExecution";
import ExperienceDetail from "../components/experience/ExperienceDetail";
import ExperienceItemToWorker from "../components/experience/ExperienceItemToWorker";
import ExperienceDetail2 from "../components/experience/ExperienceDetail2";
import EquipmentDoc from "../components/equipment/equInfo/EquipmentDoc";
import EquipmentInfoCard from "../components/equipment/equInfo/EquipmentInfoCard";
import EquipmentUse from "../components/equipment/equInfo/EquipmentUse";
import EquipmentRepair from "../components/equipment/equInfo/EquipmentRepair";
import EquipmentPicture from "../components/equipment/equInfo/EquipmentPicture";
import EquipmentMaintainLog from "../components/equipment/equInfo/EquipmentMaintainLog";
import EquipmentMap from "../components/equipment/EquipmentMap";
import RepRep from "../components/rep/RepRep";

Vue.use(Router)

//解决路由重复跳转报错
const VueRouterPush = Router.prototype.push
Router.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/Login'
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Panel',
      name: 'Panel',
      component: Panel
    },
    {
      path: '/repUpload',
      name: 'repUpload',
      component: repUpload
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home,
      children: [
        {
          path: '/NoAuth',
          name: 'NoAuth',
          component: NoAuth
        },
        {
          path: '/Personal',
          name: 'Personal',
          component: Personal
        },
        {
          path: '/SysUser',
          name: 'SysUser',
          component: SysUser
        },
        {
          path: '/SysRole',
          name: 'SysRole',
          component: SysRole
        },
        {
          path: '/SysAuth',
          name: 'SysAuth',
          component: SysAuth
        },
        {
          path: '/SysGroup',
          name: 'SysGroup',
          component: SysGroup
        },
        {
          path: '/SysUserProfile',
          name: 'SysUserProfile',
          component: SysUserProfile
        },
        {
          path: '/CheckingPlan',
          name: 'CheckingPlan',
          component: CheckingPlan
        },
        {
          path: '/CheckingTask',
          name: 'CheckingTask',
          component: CheckingTask
        },
        {
          path:'/CheckingItem',
          name:'CheckingItem',
          component:CheckingItem
        },
        {
          path:'/ExecutionRule',
          name:'ExecutionRule',
          component:ExecutionRule
        },
        {
          path:'/CheckingPlanAdd',
          name:'CheckingPlanAdd',
          component:()=>import('../components/checking/CheckingPlanAdd')
        },
        {
          path:'/CheckingTaskExecution',
          name:'CheckingTaskExecution',
          component:()=>import('../components/checking/CheckingTaskExecution')
        },
        {
          path:'/CheckingTaskDetail',
          name:'CheckingTaskDetail',
          component:()=>import('../components/checking/CheckingTaskDetail'),
          children:[
            {
              path:'/CheckingTaskInfo',
              name: 'CheckingTaskInfo',
              component:()=>import('../components/checking/CheckingTaskInfo')
            },
            {
              path:'/CheckingTaskItem',
              name: 'CheckingTaskItem',
              component:()=>import('../components/checking/CheckingTaskItem')
            },
            {
              path:'/CheckingTaskLog',
              name: 'CheckingTaskLog',
              component:()=>import('../components/checking/CheckingTaskLog')
            },
            {
              path:'/CheckingTaskFile',
              name: 'CheckingTaskFile',
              component:()=>import('../components/checking/CheckingTaskFile')
            }
          ]
        },
        {
          path:'/CheckingPlanDetail',
          name:'CheckingPlanDetail',
          component:()=>import('../components/checking/CheckingPlanDetail'),
          children:[
            {
              path:'/CheckingPlanEquipment',
              name:'CheckingPlanEquipment',
              component:()=>import('../components/checking/CheckingPlanEquipment')
            },
            {
              path:'/CheckingPlanFile',
              name:'CheckingPlanFile',
              component:()=>import('../components/checking/CheckingPlanFile')
            },
            {
              path:'/CheckingPlanInfo',
              name:'CheckingPlanInfo',
              component:()=>import('../components/checking/CheckingPlanInfo')
            },
            {
              path:'/CheckingPlanLog',
              name:'CheckingPlanLog',
              component:()=>import('../components/checking/CheckingPlanLog')
            },{
              path:'/CheckingPlanItem',
              name:'CheckingPlanItem',
              component: ()=>import('../components/checking/CheckingPlanItem')
            }
          ]
        },
        {
          path: '/RepList',
          name: 'RepList',
          component: RepList
        },
        {
          path: '/RepToDo',
          name: 'RepToDo',
          component: RepToDo
        },
        {
          path: '/RepDone',
          name: 'RepDone',
          component: RepDone
        },
        {
          path: '/RepAdd',
          name: 'RepAdd',
          component: RepAdd,
          children:[
            {
              path: '/RepAddP1',
              name: 'RepAddP1',
              component: RepAddP1
            }
          ]
        },
        {
          path: '/RepDetail',
          name: 'RepDetail',
          component: RepDetail,
          children:[
            {
              path: '/RepPlan',
              name: 'RepPlan',
              component: RepPlan
            },
            {
              path: '/RepCom',
              name: 'RepCom',
              component: RepCom
            },
            {
              path: '/RepFile',
              name: 'RepFile',
              component: RepFile
            },
            {
              path: '/RepPart',
              name: 'RepPart',
              component: RepPart
            },
            {
              path: '/RepLog',
              name: 'RepLog',
              component: RepLog
            },
          ]
        },
        //保养计划列表模块
        {
          path: '/MaintainPlan',
          name: 'MaintainPlan',
          component: MaintainPlan
        },
        //保养计划增加
        {
          path: '/MaintainPlanAdd',
          name: 'MaintainPlanAdd',
          component: MaintainPlanAdd
        },
        //保养计划详情
        {
          path:'/MaintainPlanDetail',
          name:'MaintainPlanDetail',
          component:maintainPlanDetail,
          children:[
            {
              path: '/maintainPlanInfo',
              name: 'maintainPlanInfo',
              component: maintainPlanInfo
            },
            {
              path: '/maintainPlanEquipment',
              name: 'maintainPlanEquipment',
              component: maintainPlanEquipment
            },
            {
              path: '/maintainPlanItem',
              name: 'maintainPlanItem',
              component: maintainPlanItem
            },
            {
              path: '/maintainPlanFile',
              name: 'maintainPlanFile',
              component: maintainPlanFile
            },
            {
              path: '/maintainPlanLog',
              name: 'maintainPlanLog',
              component: maintainPlanLog
            },
            {
              path: '/maintainPlanPart',
              name: 'maintainPlanPart',
              component: maintainPlanPart
            },
          ]
        },
        {
          path: '/maintainTask',
          name: 'maintainTask',
          component: maintainTask
        },
        {
          path: '/MaintainTaskExecution',
          name: 'MaintainTaskExecution',
          component: MaintainTaskExecution
        },
        {
          path:'/MaintainTaskDetail',
          name:'MaintainTaskDetail',
          component:MaintainTaskDetail,
          children:[
            {
              path:'/MaintainTaskInfo',
              name:'MaintainTaskInfo',
              component:MaintainTaskInfo,
            },
            {
              path:'/MaintainTaskItem',
              name:'MaintainTaskItem',
              component:MaintainTaskItem,
            },
            {
              path:'/MaintainTaskPart',
              name:'MaintainTaskPart',
              component:MaintainTaskPart,
            },
            {
              path:'/MaintainTaskFile',
              name:'MaintainTaskFile',
              component:MaintainTaskFile,
            },
            {
              path:'/MaintainTaskLog',
              name:'MaintainTaskLog',
              component:MaintainTaskLog,
            },
          ]
        },
        {
          path: '/MaintainItem',
          name: 'MaintainItem',
          component: MaintainItem
        },
        {
          path: '/MaintainLevel',
          name: 'MaintainLevel',
          component: MaintainLevel
        },
        {
          path:'/PartType',
          name:'PartType',
          component:PartType,
        },
        {
          path:'/EquType',
          name:'EquType',
          component:EquType,
        },
        {
          path:'/EquipmentInfo',
          name:'EquipmentInfo',
          component:EquipmentInfo,
          children:[
            {
              path: '/EquipmentDoc',
              name: 'EquipmentDoc',
              component: EquipmentDoc
            },
            {
              path: '/EquipmentInfoCard',
              name: 'EquipmentInfoCard',
              component: EquipmentInfoCard
            },
            {
              path: '/EquipmentUse',
              name: 'EquipmentUse',
              component: EquipmentUse
            },
            {
              path: '/EquipmentRepair',
              name: 'EquipmentRepair',
              component: EquipmentRepair
            },
            {
              path: '/EquipmentPicture',
              name: 'EquipmentPicture',
              component: EquipmentPicture
            },
            {
              path: '/EquipmentMaintainLog',
              name: 'EquipmentMaintainLog',
              component: EquipmentMaintainLog
            },

          ]
        },
        {
          path:'/EquipmentList',
          name:'EquipmentList',
          component:EquipmentList,
        },

        {

          path:'/ExperienceItemToWorker',
          name:'ExperienceItemToWorker',
          component:ExperienceItemToWorker,
          children:[
            {
              path:'/ExperienceDetail',
              name:'ExperienceDetail',
              component:ExperienceDetail,
            },
          ]
        },
        {
          path:'/ExperienceDetail2',
          name:'ExperienceDetail2',
          component:ExperienceDetail2,
        },
        {
          path: '/EquipmentAdd',
          name: 'EquipmentAdd',
          component: EquipmentAdd,
          children:[
            {
              path: '/EquipmentMap',
              name: 'EquipmentMap',
              component: EquipmentMap
            },
          ]
        },
        {
          path:'/PartInfo',
          name:'PartInfo',
          component:PartInfo,
        },
        {
          path: '/PartAdd',
          name: 'PartAdd',
          component: PartAdd
        },
        {
          path: '/RepRep',
          name: 'RepRep',
          component: RepRep
        },
        {
          path: '/PartInfoDetail',
          name: 'PartInfoDetail',
          component: PartInfoDetail,
          children: [
            {
              path: '/PartInfoCard',
              name: 'PartInfoCard',
              component: PartInfoCard
            },
            {
              path: '/PartInventory',
              name: 'PartInventory',
              component: PartInventory
            },
            {
              path: '/PartInStorageDetail',
              name: 'PartInStorageDetail',
              component: PartInStorageDetail
            },
            {
              path: '/PartOutStorageDetail',
              name: 'PartOutStorageDetail',
              component: PartOutStorageDetail
            },
            {
              path: '/PartAssociatedEqu',
              name: 'PartAssociatedEqu',
              component: PartAssociatedEqu
            },
            {
              path: '/PartImg',
              name: 'PartImg',
              component: PartImg
            },
            {
              path: '/PartAssociatedDoc',
              name: 'PartAssociatedDoc',
              component: PartAssociatedDoc
            },
            {
              path: '/PartOperateRecord',
              name: 'PartOperateRecord',
              component: PartOperateRecord
            },
          ]
        },
        {
          path: '/OutStorageDetail',
          name: 'OutStorageDetail',
          component: OutStorageDetail,
          children: [
            {
              path: '/OutStorageInfo',
              name: 'OutStorageInfo',
              component: OutStorageInfo,
            },
            {
              path: '/OutStoragePartInfo',
              name: 'OutStoragePartInfo',
              component: OutStoragePartInfo,
            },
            {
              path: '/OutStorageLog',
              name: 'OutStorageLog',
              component: OutStorageLog,
            },
          ],
        },

        {
          path: '/InStorageDetail',
          name: 'InStorageDetail',
          component: InStorageDetail,
          children: [
            {
              path: '/InStorageInfo',
              name: 'InStorageInfo',
              component: InStorageInfo,
            },
            {
              path: '/InStoragePartInfo',
              name: 'InStoragePartInfo',
              component: InStoragePartInfo,
            },
            {
              path: '/InStorageLog',
              name: 'InStorageLog',
              component: InStorageLog,
            },
          ],
        },

        {
          path:'/PartInStorage',
          name:'PartInStorage',
          component:PartInStorage,
        },
        {
          path:'/PartOutStorage',
          name:'PartOutStorage',
          component:PartOutStorage,

        },
        {
          path: '/OutStorageAdd',
          name: 'OutStorageAdd',
          component: OutStorageAdd
        },
        {
          path: '/InStorageAdd',
          name: 'InStorageAdd',
          component: InStorageAdd
        },

        {
          path:'/ExperienceItem',
          name:'ExperienceItem',
          component:ExperienceItem,
          children:[

          ]
        },
        {
          path:'/ExperienceAdd',
          name:'ExperienceAdd',
          component:ExperienceAdd,
        },
        {
          path: '/Editor',
          name: 'Editor',
          component: Editor
        },
      ]
    }
  ]
})

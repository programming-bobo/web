import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"
import Print from "vue-print-nb";
import Vue from "vue";
//使用vuex插件
Vue.use(Vuex)
Vue.use(Print);
export default new Vuex.Store({
  //全局状态
  state: {
    //用户身份令牌
    token: '',
    //当前登录用户名
    loginUserName: '',
    loginUserId:'',
    //激活的菜单
    menuIndex: '',
    //一级菜单
    breadcrumbTitle: '',
    //二级菜单
    breadcrumbIndex: '',
    //头像地址
    headImgUrl:'',
    //权限列表
    authUrlsList:'',
    //保养计划数据
    maintainPlanData:'',
    //点检计划信息
    checkingPlanId:'',
    checkingPlanName:'',
    checkingPlanStatus:'',
    //点检计划激活的菜单
    checkingPlanMenuIndex:'',
    //点检任务信息
    checkingTaskId:'',
    checkingTaskName:'',
    checkingTaskStatus:'',
    //点检任务激活的菜单
    checkingTaskMenuIndex:'',
    maintainPlanDataId:'',
    repIndex:'',
    partInfoId: '',
    //  经验id
    expId:'',
    //设备名称
    equName:'',
    //设备负责人
    equInChargeName:'',
    //设备负责人电话
    equPhone:'',
    //设备ID（自增主键）
    equId:'',
    //设备编号
    eId:'',
    //保养计划激活的菜单
    maintainPlanMenuIndex:'',
    maintainTaskDataId:'',
    //出库明细信息
    partOutStorageDetail: {},
    //入库明细信息
    partInStorageDetail: {},
    //user自增id
    sysUserId:'',

    maintainTaskDataName:'',
    maintainPlanDataName:'',
    maintainTaskMenuIndex:'',
    maintainTaskPartRequire:'',
    //父子传参变量
    TinymceContent:'<head>\n' +
      '  <meta charset="UTF-8">\n' +
      '  <title>Title</title>\n' +
      '</head>',

    xPosition:'',
    yPosition:''
  },
  //同步数据提交
  mutations: {
    token(state, param) {
      state.token = param
    },
    loginUserName(state, param) {
      state.loginUserName = param
    },
    loginUserId(state, param) {
      state.loginUserId = param
    },
    menuIndex(state, param) {
      state.menuIndex = param
    },
    breadcrumbTitle(state, param) {
      state.breadcrumbTitle = param
    },
    breadcrumbIndex(state, param) {
      state.breadcrumbIndex = param
    },
    headImgUrl(state, param) {
      state.headImgUrl = param
    },
    authUrlsList(state, param) {
      state.authUrlsList = param
    },
    maintainPlanData(state, param) {
      state.maintainPlanData = param
    },
    checkingPlanId(state, param) {
      state.checkingPlanId = param
    },
    checkingPlanName(state, param) {
      state.checkingPlanName = param
    },
    checkingPlanStatus(state, param) {
      state.checkingPlanStatus = param
    },
    checkingPlanMenuIndex(state, param) {
      state.checkingPlanMenuIndex = param
    },
    checkingTaskId(state, param) {
      state.checkingTaskId = param
    },
    checkingTaskName(state, param) {
      state.checkingTaskName = param
    },
    checkingTaskStatus(state, param) {
      state.checkingTaskStatus = param
    },
    checkingTaskMenuIndex(state, param) {
      state.checkingTaskMenuIndex = param
    },
    maintainPlanDataId(state, param) {
      state.maintainPlanDataId = param
    },
    repIndex(state, param) {
      state.repIndex = param
    },
    partInfoId(state, param){
      state.partInfoId = param
    },
    expId(state, param) {
      state.expId = param
    },
    equName(state, param){
      state.equName = param
    },
    equInChargeName(state, param){
      state.equInChargeName = param
    },
    equPhone(state, param){
      state.equPhone = param
    },
    equId(state, param){
      state.equId = param
    },
    eId(state, param){
      state.eId = param
    },
    maintainPlanMenuIndex(state, param){
      state.maintainPlanMenuIndex = param
    },
    maintainTaskDataId(state, param){
      state.maintainTaskDataId = param
    },
    partOutStorageDetail(state, param) {
      state.partOutStorageDetail = param;
    },
    maintainTaskDataName(state, param){
      state.maintainTaskDataName = param
    },
    maintainPlanDataName(state, param) {
      state.maintainPlanDataName = param;
    },
    maintainTaskMenuIndex(state, param) {
      state.maintainTaskMenuIndex = param;
    },
    maintainTaskPartRequire(state, param) {
      state.maintainTaskPartRequire = param;
    },
    sysUserId(state, param){
      state.sysUserId=param;
    },
    TinymceContent(state,param){
      state.TinymceContent=param;
    },
    partInStorageDetail(state, param) {
      state.partInStorageDetail = param;
    },
    xPosition(state,param){
      state.xPosition=param;
    },
    yPosition(state,param){
      state.yPosition=param;
    },
  },
  //引入持久化组件
  plugins: [createPersistedState()]
})

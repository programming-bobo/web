import axios from 'axios'
import store from "../store";
// 是否显示重新登录
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: '/api',
  // 超时
  timeout: 10000,
  headers: {
    Authorization: store.state.token
  }
})

export default service
